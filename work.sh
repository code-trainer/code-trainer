npm run build

git add .

if [ -z "$1" ]; then
  git commit --no-verify --no-status --author="code-trainer <smadjaros@bk.ru>" -m "Update"
else
  git commit --no-verify --no-status --author="code-trainer <smadjaros@bk.ru>" -m "$1"
fi

git push