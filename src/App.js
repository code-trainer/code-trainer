import React from 'react';
import { StartScreen } from './Components/Screens/StartScreen';
import { PreviewScreen } from './Components/Screens/PreviewScreen';
import { ChallengeScreen } from './Components/Screens/ChallengeScreen';
import { ResultScreen } from './Components/Screens/ResultScreen';
import { mockFileContent } from './Data/mockFileContent';


export const App = () => {
  const [pathToFile, setPathToFile] = React.useState('');
  const [screen, setScreen] = React.useState('start');
  const [fileContent, setFileContent] = React.useState('');
  const [resultCode, setResultCode] = React.useState('');

  const renderScreen = name => {
    if (name === 'preview') {
      return <PreviewScreen
          changeScreen={newScreenName => setScreen(newScreenName)}
          pathToFile={pathToFile}
          fileContent={fileContent}
        />;
    } else if (name === 'challenge') {
      return <ChallengeScreen
        changeScreen={newScreenName => setScreen(newScreenName)}
        pathToFile={pathToFile}
        fileContent={fileContent}
        setResultCode={newResultCode => setResultCode(newResultCode)}
      />
    } else if (name === 'result') {
      return <ResultScreen
        changeScreen={newScreenName => setScreen(newScreenName)}
        pathToFile={pathToFile}
        fileContent={fileContent}
        resultCode={resultCode}
      />
    } else {
      return <StartScreen
        changeScreen={newScreenName => setScreen(newScreenName)}
        pathToFile={pathToFile}
        setPathToFile={newPathToFile => setPathToFile(newPathToFile)}
        setFileContent={newFileContent => setFileContent(newFileContent)}
        setResultCode={newResultCode => setResultCode(newResultCode)}
      />;
    }
  }
  
  return (
    <div>
      {renderScreen(screen)}
    </div>
  );
}
