import React from 'react';
import { ScreenContainer } from './../../ScreenContainer';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import CircularProgress from '@material-ui/core/CircularProgress';

export const PreviewScreen = (props) => {
  const previewer = <textarea defaultValue={props.fileContent} className="previewer code-container" spellCheck="false"></textarea>;

  const handleBack = () => {
    props.changeScreen('start');
  }

  const handleForward = () => {
    props.changeScreen('challenge');
  }

  return (
    <ScreenContainer>
      <Box className="header">
        <Button variant="contained" onClick={handleBack} className="back-btn">Back</Button>
        <Link href={props.pathToFile} target="_blank">{props.pathToFile}</Link>
      </Box>
      {!props.fileContent ? <CircularProgress className="centered"/> : previewer}
      <Box className="footer">
        <Button variant="contained" color="primary" onClick={handleForward} className="forward-btn">Go</Button>
      </Box>
    </ScreenContainer>
  );
}
