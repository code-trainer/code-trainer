import React from 'react';
import { ScreenContainer } from './../../ScreenContainer';
import Button from '@material-ui/core/Button';
import { arrayOfFiles } from './../../../Data/arrayOfFiles';
import { getRandomElement } from './../../../Utils/getRandomElement';


export const StartScreen = (props) => {
  //Reset state
  props.setPathToFile('');
  props.setFileContent('');
  props.setResultCode('');

  const handleClick = () => {
    const pathToFile = getRandomElement(arrayOfFiles);
    props.setPathToFile(pathToFile);
    fetch(pathToFile)
      .then(data => {
        data.text()
          .then(content => {
            props.setFileContent(content);
          })
      })
      .catch((error) => {
        console.error('Error:', error);
      });
    props.changeScreen('preview');
  };

  return (
    <ScreenContainer>
      <Button variant="contained" color="primary" onClick={handleClick} className="centered">
        START
      </Button>
    </ScreenContainer>
  );
}
