import React from 'react';
import { ScreenContainer } from './../../ScreenContainer';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { Typography } from '@material-ui/core';
import Draggable from 'react-draggable';
import { getSimilarity } from './../../../Utils/getSimilarity';


export const ResultScreen = (props) => {
  const repoCodeWithoutEmptyLines = props.fileContent.split('\n').filter(line => { return line !== ''}).join('\n');
  const resultCodeWithoutEmptyLines = props.resultCode.split('\n').filter(line => { return line !== ''}).join('\n');
  
  const similarityPercentage = parseInt(getSimilarity(resultCodeWithoutEmptyLines, repoCodeWithoutEmptyLines) * 100);

  const handleStartAgain = () => {
    props.changeScreen('start');
  }

  return (
    <ScreenContainer>
      <Link href={props.pathToFile} target="_blank" className="link-to-source">Source</Link>
      <Box className="header">
        <Typography variant="h6" component="p">Your Code (draggable)</Typography>
        <Typography variant="h6" component="p">Repo Code</Typography>
      </Box>
      <Box className="compare-container">
        <Draggable>
          <textarea defaultValue={resultCodeWithoutEmptyLines} className="code-container draggable" spellCheck="false"></textarea>
        </Draggable>
        <textarea defaultValue={repoCodeWithoutEmptyLines} className="code-container" spellCheck="false"></textarea>
      </Box>
      <Box className="footer">
        <Typography  variant="h6" component="p">Accuracy {similarityPercentage}%</Typography>
        <Button variant="contained" color="primary" onClick={handleStartAgain} className="forward-btn">Start Again</Button>
      </Box>
    </ScreenContainer>
  );
}
