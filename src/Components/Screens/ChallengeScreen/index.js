import React from 'react';
import { ScreenContainer } from './../../ScreenContainer';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import { getArrayShuffled } from './../../../Utils/getArrayShuffled';
import Button from '@material-ui/core/Button';


const SortableItem = SortableElement(({value}) => <pre className="draggable-item">{value}</pre>);

const SortableList = SortableContainer(({items}) => {
  return (
    <Box className="challenge-code-container">
      {items.map((value, index) => (
        <SortableItem key={`item-${Math.random()}`} index={index} value={value} />
      ))}
    </Box>
  );
});

export const ChallengeScreen = (props) => {
  const initialArrayOfLines = getArrayShuffled(props.fileContent.split('\n'));

  const [lines, setLines] = React.useState({items: initialArrayOfLines});

  const handleSortEnd = ({oldIndex, newIndex}) => {
    setLines((lines) => ({
      items: arrayMove(lines.items, oldIndex, newIndex),
    }));
  };

  const handleBack = () => {
    props.changeScreen('preview');
  }

  const handleSubmit = () => {
    const resultCode = lines.items.join('\n');
    props.setResultCode(resultCode);
    props.changeScreen('result');
  }

  return (
    <ScreenContainer>
      <Box className="header">
        <Button variant="contained" onClick={handleBack} className="back-btn">Back</Button>
        <Link href={props.pathToFile} target="_blank">{props.pathToFile}</Link>
      </Box>
      <SortableList items={lines.items} onSortEnd={handleSortEnd} />
      <Box className="footer">
        <Button variant="contained" color="primary" onClick={handleSubmit} className="forward-btn">Submit</Button>
      </Box>
    </ScreenContainer>
  );
}
