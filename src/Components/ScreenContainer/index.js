import React from 'react';
import './index.css';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'start',
    height: '100vh',
    background: 'black',
    color: 'green',
  },
});

export const ScreenContainer = (props) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {props.children}
    </Box>
  );
}
