const fs = require('file-system');
const request = require('request');

const listOfRepos = [
  'https://github.com/mui-org/material-ui',
  'https://github.com/checkly/puppeteer-examples',
  'https://github.com/hicapacity/nodejs-examples/',
  'https://github.com/michelou/nodejs-examples/',
  'https://github.com/PacktPublishing/Node.js-By-Example/'
];

const getContentOfFile = pathToFile => {
  return fs.readFileSync(pathToFile, 'utf-8', (err, data) => {
    if (err) throw err;
    return data;
  })
};

const credsStr = getContentOfFile('./../.creds/gh');
const creds = {
  user: credsStr.split(':')[0],
  token: credsStr.split(':')[1]
};

const connections = {
  open: 0,
  closed: 0
};

const arrayOfJSFiles = [];

const fetchJSFiles = (repo, dirName) => {
  const options = {
    url: `https://api.github.com/repos/${repo}/contents/${dirName}`,
    headers: {'user-agent': 'node.js'},
    auth: {
      'user': creds.user,
      'pass': creds.token
    }
  };
  
  function callback(error, response, body) {
    if (!error && response.statusCode === 200) {
      const responseBody = JSON.parse(body);
      responseBody.forEach(obj => {
        if (obj.type === 'file' && obj.name.endsWith('.js')) {
          arrayOfJSFiles.push(obj.download_url);
        } else if (obj.type === 'dir') {
          fetchJSFiles(repo, obj.name);
        }
      });
    } else {
      //console.error('ERROR');
      //console.log(body);
    }
    connections.closed++;
  }
  
  connections.open++;
  request(options, callback);
};

(async function () {
  const arrayOfNormalizedRepos = [];
  listOfRepos.forEach(pathToRepo => {
    if (pathToRepo.length) {
      let repoName = pathToRepo.startsWith('https://github.com/') ? pathToRepo.split('https://github.com/')[1] : pathToRepo;
      if (repoName.endsWith('/')) {
        repoName = repoName.substring(0, repoName.length - 1);
      }
      if (!arrayOfNormalizedRepos.includes(repoName)) {
        arrayOfNormalizedRepos.push(repoName);
      }
    }
  });

  arrayOfNormalizedRepos.forEach(repo => {
    console.log(`Working on ${repo}`);
    fetchJSFiles(repo, '');
  });

  const interval = setInterval(() => {
    if (connections.open === connections.closed) {
      clearInterval(interval);
      const outputArray = arrayOfJSFiles.sort();
      fs.writeFile('output.txt', outputArray.join('\n'), function(err) {});
      console.log(`All done in ${connections.closed} requests.`);
    }
  }, 1000);
}());
